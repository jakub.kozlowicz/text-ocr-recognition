"""Train HTR network."""

import os

import hydra
from omegaconf import DictConfig, OmegaConf
import pydantic
import pytorch_lightning as pl
import torch
from torch.utils.data import DataLoader, random_split

from ocr.config.config import OCRConfig
from ocr.dataloaders.common import construct_image_path_from_identifier, exclude_wrong_data
from ocr.dataloaders.words import loads_words_database
from ocr.datasets.dataset import HtrDataset
from ocr.datasets.labels_encoder import LabelEncoder
from ocr.datasets.transformations import images, labels
from ocr.losses.ctc import CTCLoss
from ocr.networks.base import BaseHtrModule
from ocr.networks.words import WordsHtrModule


@hydra.main(config_path="config", config_name="config", version_base="1.3")
def main(config: DictConfig) -> None:
    """Main entrypoint."""
    OmegaConf.resolve(config)
    cfg = pydantic.parse_obj_as(OCRConfig, config)

    words_db = loads_words_database(
        cfg.paths.labels.words,
        cfg.paths.images.words,
        construct_image_path_from_identifier,
    )

    words_db = exclude_wrong_data(words_db)

    label_encoder = LabelEncoder()

    words_dataset = HtrDataset(
        words_db,
        [images.ResizeImage(cfg.params.word_image_height, cfg.params.word_image_width)],
        [
            labels.LabelPadder(
                width=max([len(word.text) for word in words_db]),
                value=label_encoder.blank_char_value,
            ),
        ],
        label_encoder,
    )

    train, validation = random_split(words_dataset, [cfg.params.test_size, cfg.params.test_size])

    words_model = BaseHtrModule(
        WordsHtrModule(label_encoder.num_classes, dropout=cfg.params.dropout),
        CTCLoss(blank_char=label_encoder.blank_char_value),
        label_encoder.CLASSES,
        cfg.params.lr,
    )

    trainer = pl.Trainer(max_epochs=cfg.params.epoch_count)
    trainer.fit(
        words_model,
        DataLoader(
            train,
            batch_size=cfg.params.batch_size,
            shuffle=True,
            num_workers=os.cpu_count() or 1,
        ),
        DataLoader(
            validation,
            batch_size=cfg.params.batch_size,
            shuffle=False,
            num_workers=os.cpu_count() or 1,
        ),
    )

    if not cfg.paths.models.words.exists():
        cfg.paths.models.words.mkdir(parents=True)

    torch.save(words_model, cfg.paths.models.words)


if __name__ == "__main__":
    main()
