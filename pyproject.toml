[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"

[tool.poetry]
name = "ocr"
version = "0.1.0"
description = "Text recognition using neural networks and computer vision."
authors = ["Jakub Kozłowicz <ja.kozlowicz@gmail.com>"]
readme = "README.md"
license = "MIT"
packages = [{ include = "ocr", from = "src" }]

[tool.poetry.dependencies]
python = "^3.11"
torch = "^2.1.0"
pytorch-lightning = "^2.1.0"
numpy = "^1.24.0"
opencv-python = "^4.7.0.72"
opencv-stubs = "^0.0.7"
pydantic = "^1.10.7"
hydra-core = "^1.3.2"
hydra-colorlog = "^1.2.0"
torchvision = "^0.16.0"
matplotlib = "^3.7.1"
tensorboard = "^2.13.0"
onnxruntime = "^1.15.0"


[tool.poetry.group.dev.dependencies]
black = "^23.1.0"
isort = "^5.12.0"
mypy = "^1.1.1"
taskipy = "^1.10.3"
ruff = "^0.0.259"

[tool.isort]
profile = "black"
src_paths = ["src", "tests"]
skip = [".gitignore"]
extend_skip = [".md", ".rst", ".json", ".txt"]
line_length = 100
lines_after_imports = 2
lines_between_types = 1
force_alphabetical_sort_within_sections = true
force_sort_within_sections = true


[tool.black]
color = true
line-length = 100
target-version = ['py311']
skip-string-normalization = true


[tool.ruff]
line-length = 120
extend-select = [
    "N", # pep8-naming
    "F", # pyflakes
    "E", # pycodestyle errors
    "W", # pycodestyle warnings
    "F", # pyflakes
    "UP", # pyupgrade
    "PLC", # pylint-convention
    "PLE", # pylint-error
    "PLR", # pylint-refactor
    "PLW", # pylint-warning
    # "I",   # isort
    # "ANN", # flake8-annotations
    "A", # flake8-builtins
    "ARG", # flake8-unused-arguments
    "B", # flake8-bugbear
    "C4", # flake8-comprehensions
    "COM", # flake8-commas
    "DTZ", # flake8-datetimez
    "EM", # flake8-errmsg
    "G", # flake8-logging-format
    "ICN", # flake8-import-conventions
    "ISC", # flake8-implicit-str-concat
    "PIE", # flake8-pie
    "PTH", # flake8-use-pathlib
    "RET", # flake8-return
    "RSE", # flake8-raise
    "S", # flake8-bandit
    "SIM", # flake8-simplify
    "TID", # flake8-tidy-imports
    "RUF", # Ruff-specific rules
]
mccabe = { max-complexity = 14 }
isort = { known-first-party = ["ocr"] }
target-version = "py311"

[tool.flake8]
exclude = ".venv"
max-line-length = 100


[tool.mypy]
strict = true
ignore_missing_imports = true
check_untyped_defs = true


[tool.pyright]
include = ["src/ocr"]
exclude = [".venv"]
venvPath = "."
venv = ".venv"
reportMissingImports = true
reportMissingTypeStubs = false


[tool.taskipy.tasks]
format = "isort ./src/ocr && black ./src/ocr"
lint = """\
  black --check ./src/ocr && \
  ruff check ./src/ocr && \ 
  mypy ./src/ocr \
"""
