\section{Program}

\subsection{Environment and used software}

The environment and software used in this project consisted of several
components, leveraging popular libraries and frameworks for efficient and
effective implementation of handwriting text recognition.

The project primarily utilized Python, a widely adopted programming language in
the field of machine learning and artificial intelligence. For deep learning
tasks, the project employed PyTorch, a powerful open-source machine learning
framework. PyTorch provides a flexible and intuitive interface for building and
training neural networks. To enhance the training process and streamline
experimentation, the project utilized PyTorch Lightning. PyTorch Lightning is a
lightweight PyTorch wrapper that simplifies the training loop implementation,
reduces boilerplate code, and provides additional features such as distributed
training and mixed precision training. For configuration management, the
project employed Hydra, a powerful framework for managing complex configuration
hierarchies. In addition to the Hydra library the project use Pydantic as data
validation and parsing library. In terms of image processing, the project
utilized OpenCV, a popular computer vision library. OpenCV provided a wide
range of functions and algorithms for image preprocessing, such as image
resizing, normalization, and noise removal.

\subsection{Dataset loading and data preparation}

During the training phase, a subset of the IAM dataset was utilized. The
dataset was obtained by loading data from an ASCII file that contained the file
paths to the images along with their corresponding textual content. To ensure
effective model training, the dataset was divided into training and validation
sets. The validation set played a crucial role in monitoring the training
process and mitigating overfitting.

To facilitate data loading and manipulation, the PyTorch Dataset class was
employed. This abstract class provided a standardized interface for accessing
the dataset. To tailor the dataset to our specific needs, a custom dataset
class was developed by extending the Dataset class. This custom dataset class
enabled the loading of both the images and their associated text.

Subsequently, a PyTorch DataLoader class was instantiated using the custom
dataset class. The DataLoader class acted as an iterable, offering a convenient
mechanism for accessing the data in batches. Two separate DataLoader instances
were created: one for training and another for validation purposes. The
training DataLoader facilitated the model training process, while the
validation DataLoader served to monitor the training procedure and prevent
overfitting.

During data loading, a batch size of 64 images was utilized. To ensure
uniformity, the images were resized to dimensions of 128\( \times \)32 pixels
(keeping the aspect ratio with black padding). Furthermore, a normalization
step was performed, wherein the images were adjusted using the mean and
standard deviation calculated from the dataset.

Regarding the textual content, it underwent encoding using the CTC
(Connectionist Temporal Classification) loss algorithm. Following encoding, the
text was transformed into a tensor and padded to match the maximum text length
within the dataset. The padded text was subsequently converted to another
tensor and employed as the target for the CTC loss algorithm.

\subsection{Network architecture}

The architecture of the network used in project is a combination of
Convolutional Neural Networks (CNNs), Recurrent Neural Networks (RNNs),
specifically a Long Short-Term Memory (LSTM) network, and fully connected
layers. The purpose of the architecture is to recognize handwritten words
(HTR). Full network architecture is shown on figure~\ref{fig:network-architecture}.
\begin{figure}[hbpt]
      \begin{center}
            \includegraphics[width=\textwidth]{figures/network2.pdf}
      \end{center}
      \caption{Architecture of the network.}%
      \label{fig:network-architecture}
\end{figure}

\begin{enumerate}
      \item \textbf{Convolutional Blocks for Feature Extraction}: The network
            starts with a series of convolutional layers contained within
            \texttt{Residual} blocks for feature extraction. As the data propagates
            through these layers, low-level features (like edges, corners, etc.)
            get transformed into more abstract and complex features. The
            convolutional layers also progressively increase the depth of the
            feature maps (up to 64), facilitating the learning of more complex
            features. The \texttt{Residual} blocks, through their shortcut
            connections, help mitigate the problem of vanishing gradients during
            training, especially in deep networks.
      \item \textbf{Tensor Reshaping}: After passing through the convolutional
            blocks, the tensor's dimensions are reshaped before it can be input to
            the LSTM layer. The reshaping is necessary because the LSTM expects
            input in the form (\texttt{batch\_size}, \texttt{sequence\_length},
            \texttt{number\_of\_features}), which differs from the output shape of
            the convolutional layers.
      \item \textbf{Recurrent Block for Sequence Processing}: The reshaped tensor is
            then input into a bidirectional LSTM layer. The LSTM layer treats the
            input as a sequence, where order matters. This is crucial for tasks
            like handwriting recognition where the order of the characters in a
            word is important. The LSTM layer captures the temporal dependencies in
            the sequence data. Being bidirectional means it processes the data in
            both directions (forward and backward), helping it capture both past
            and future context.
      \item \textbf{Dropout for Regularization}: The output from the LSTM layer is
            then passed through a dropout layer, which randomly sets a fraction of
            input units to 0 at each update during training time. This helps
            prevent overfitting by adding a regularization effect.
      \item \textbf{Fully Connected Layer for Class Scores}: The output from the
            dropout layer is passed to a fully connected (linear) layer. This layer
            transforms the LSTM outputs to the final class scores. Each unit in
            this layer calculates a score indicating the confidence of the network
            that the input image contains a specific character.
      \item \textbf{Softmax for Probability Distributions}: Finally, a LogSoftmax
            layer calculates the logarithm of the softmax of these scores. The
            softmax operation transforms the scores into probability distributions
            for each possible output class, and the logarithm is often used to
            improve numerical stability. These log-probabilities can then be used
            with a loss function like negative log-likelihood for network training.
\end{enumerate}

\subsection{Training and validation}

Training and validation were performed using the PyTorch Lightning framework.
The training process was carried out using a single NVIDIA A10 GPU\@. The training
was performed for 350 epochs, with the learning rate set to 0.002. The Adam
optimizer was employed, and the CTC loss function was utilized. Consecutive
steps of the training program describe following steps as well as
figure~\ref{fig:training-flow}.
\begin{figure}[hbpt]
      \begin{center}
            \includegraphics[width=0.45\textwidth]{figures/training-flow.pdf}
      \end{center}
      \caption{Training program flow.}%
      \label{fig:training-flow}
\end{figure}

\begin{enumerate}
      \item \textbf{Configuration Handling}: The script employs the
            \texttt{OmegaConf} and \texttt{Pydantic} libraries to read and parse a
            configuration file that contains the training parameters and data
            paths.

      \item \textbf{Data Loading and Preprocessing}: The training data is loaded
            and preprocessed, encompassing operations such as image and label
            reading, exclusion of faulty data, image resizing, label padding, and
            label encoding. Following preprocessing, the dataset is divided into a
            training set and a validation set.

      \item \textbf{Model and Loss Function Initialization}: The training script
            instantiates a handwritten text recognition (HTR) model and configure
            the CTC loss function accordingly. Furthermore, it encapsulates both
            the model and the loss function within a \texttt{BaseHtrModule}
            LightningModule. Additionally, it provides the learning rate and the
            character classes necessary for error rate computation to this module.

      \item \textbf{PyTorch Lightning Trainer Setup}: In the next step the script
            establish a PyTorch Lightning \texttt{Trainer} with a specified number
            of epochs. Subsequently, it employs the trainer to fit the model to the
            data, utilizing the previously created training and validation sets.
            The data loaders are configured with batch sizes based on the values
            specified in the configuration file, and multiple workers are employed
            to enable parallel data loading.

      \item \textbf{Training Loop}: Throughout the training phase, the
            \texttt{training\_step} method contained in the \texttt{BaseHtrModule}
            is invoked for each batch. This method executes the model's forward
            pass, computes the loss utilizing the CTC loss function, and updates
            the character error rate metric.

      \item \textbf{Validation Loop}: After the completion of each training epoch,
            the validation loop is executed. The \texttt{validation\_step} method
            functions in a manner analogous to \texttt{training\_step}, albeit
            operating on the validation set.

      \item \textbf{Logging}: At the conclusion of each epoch, the ending callback
            methods (\texttt{on\_train\_epoch\_end},
            \texttt{on\_validation\_epoch\_end}) log the character and word error
            rates for the training and validation sets, respectively.

      \item \textbf{Optimizer Configuration}: The \texttt{configure\_optimizers}
            method defines the optimizer employed for training the model. In this
            particular case, the program utilizes the Adam optimizer, with the
            learning rate specified in the configuration file.

      \item \textbf{Model Saving}: Following the training process, the trained
            model is saved to the designated path.
\end{enumerate}


\subsubsection{Metrics}

The training and validation metrics have been recorded at the end of every
epoch. Those metrics consist of loss, character error rate and word error rate.
Figure~\ref{fig:metrics-trainig-validation} shows individual charts for each
metric for both training and validation sets.

Both metrics utilize the Levenshtein distance algorithm to calculate the error.
This algorithm calculates the minimum number of insertions, deletions, and
substitutions required to transform predicted label into true label.

\begin{figure}
      \centering
      \begin{subfigure}[t]{0.65\textwidth}
            \centering
            \includegraphics[width=\textwidth]{figures/loss.pdf}
            \caption{Loss value.}%
            \label{fig:loss-metrics}
      \end{subfigure}
      \vfill
      \begin{subfigure}[t]{0.65\textwidth}
            \centering
            \includegraphics[width=\textwidth]{figures/cer.pdf}
            \caption{Character error rate value.}%
            \label{fig:cer-metrics}
      \end{subfigure}
      \vfill
      \begin{subfigure}[t]{0.65\textwidth}
            \centering
            \includegraphics[width=\textwidth]{figures/wer.pdf}
            \caption{Word error rate value.}%
            \label{fig:wer-metrics}
      \end{subfigure}
      \caption{Metrics for the training and validation stages.}%
      \label{fig:metrics-trainig-validation}
\end{figure}

\subsection{Configuration}

For the training program the configuration shown in the
Listing~\ref{lst:config-training} has been used. It consists of two main
\begin{listing}[hbpt]
      \begin{minted}
[
baselinestretch=0.9,
frame=single,
]
{yaml}
# file: config.yaml
---
params:
  batch_size: 64
  dropout: 0.3
  epoch_count: 350
  lr: 0.002
  random_state: 314
  test_size: 0.2
  train_size: 0.8
  word_image_height: 32
  word_image_width: 128

paths:
  data: data/
  images:
    forms: data/forms
    lines: data/lines
    sentences: data/sentences
    words: data/words
  labels:
    forms: data/ascii/forms.txt
    lines: data/ascii/lines.txt
    sentences: data/ascii/sentences.txt
    words: data/ascii/words.txt
  models:
    lines: models/lines.onnx
    sentences: models/sentences.onnx
    words: models/words.onnx
\end{minted}
      \caption{Content of the configuration file for the training program.}
      \label{lst:config-training}
\end{listing}
sections. The first one --- \texttt{params}, contains parameters for the
network and training part of the program. The second one --- \texttt{paths},
contains paths to the data and models used in the program.
