"""Chart plotter for the training metrics."""

from pathlib import Path

import matplotlib.pyplot as plt
import pandas as pd


data_path = Path("loss-train.csv")
wer_train = pd.read_csv(data_path)["Value"]

iterations = pd.read_csv(data_path)["Step"]

data_path = Path("loss-test.csv")
wer_test = pd.read_csv(data_path)["Value"]

_ = plt.figure(figsize=(8, 5))
for data, label in zip((wer_test, wer_train), ("validation", "training"), strict=True):
    plt.plot(iterations, data, label=label)

plt.xlabel("Epoch number")
plt.ylabel("Value")
plt.legend(loc="upper right")
plt.title("loss")
plt.tight_layout()
plt.savefig("loss.pdf", transparent=True)
plt.show()
