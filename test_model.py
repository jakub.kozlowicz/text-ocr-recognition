"""Test HTR network."""

from random import SystemRandom

import cv2
import hydra
import numpy as np
from numpy.typing import NDArray
from omegaconf import DictConfig, OmegaConf
import pydantic

from ocr.config.config import OCRConfig
from ocr.dataloaders.common import construct_image_path_from_identifier
from ocr.dataloaders.forms import loads_forms_database
from ocr.dataloaders.lines import loads_lines_database
from ocr.image_preprocessing import detectors, transformations
from ocr.metrics.utils.text import get_cer, get_wer
from ocr.models.words import WordsModel
from ocr.text_utils import remove_spaces_before_punctuation


@hydra.main(config_path="config", config_name="config", version_base="1.3")
def main(config: DictConfig) -> None:
    """Main entrypoint."""
    OmegaConf.resolve(config)
    cfg = pydantic.parse_obj_as(OCRConfig, config)

    images_db = loads_forms_database(
        cfg.paths.labels.forms,
        cfg.paths.images.forms,
    )

    crypto = SystemRandom()
    random_image = crypto.choice(images_db)

    image = cv2.imread(str(random_image.image_path.resolve()))
    gray = transformations.convert_to_grayscale(image)
    thresh = transformations.binarize(gray)

    lines_contours = detectors.detect_lines(thresh)

    handwritten_part = transformations.split_image_vertically(
        thresh,
        sorted([0] + [line.y for line in lines_contours]),
    )[2]

    handwritten_color = transformations.split_image_vertically(
        image,
        sorted([0] + [line.y for line in lines_contours]),
    )[2]

    sorted_contours_lines = detectors.detect_text_lines(handwritten_part)
    no_lines = len(sorted_contours_lines)
    if no_lines != random_image.no_lines:
        print(f"Cannot properly detect lines. Expected {random_image.no_lines}. Got {no_lines}")
        return

    cropped_lines = transformations.extract_from_contours(handwritten_part, sorted_contours_lines)
    cropped_lines_color = transformations.extract_from_contours(
        handwritten_color,
        sorted_contours_lines,
    )

    words: list[NDArray[np.uint8]] = []
    for line_image, line_image_color in zip(cropped_lines, cropped_lines_color, strict=True):
        sorted_contours_words = detectors.detect_words_in_line(line_image)
        words += transformations.extract_from_contours(line_image_color, sorted_contours_words)

    model = WordsModel(cfg.paths.models.words)

    predicted_text = " ".join([model.predict(word) for word in words])

    lines_db = loads_lines_database(
        cfg.paths.labels.lines,
        cfg.paths.images.lines,
        construct_image_path_from_identifier,
    )

    lines_info = [line for line in lines_db if random_image.form_id in line.identifier]
    text_data = " ".join([line.text for line in lines_info])
    print("Expected text:")
    print(remove_spaces_before_punctuation(text_data))

    print("Predicted text:")
    print(remove_spaces_before_punctuation(predicted_text))

    print(
        ", ".join(
            [
                f"Image: {random_image.image_path.name}",
                f"CER: {get_cer(predicted_text, text_data):.2f}",
                f"WER: {get_wer(predicted_text, text_data):.2f}",
            ],
        ),
    )


if __name__ == "__main__":
    main()
