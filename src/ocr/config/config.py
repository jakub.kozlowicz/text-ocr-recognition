"""Configuration structures for OCR package."""

from pathlib import Path

from pydantic import BaseModel


class PathsModels(BaseModel):
    """Contains information about path for the trained models."""

    lines: Path
    sentences: Path
    words: Path


class DatabasePathsConfig(BaseModel):
    """Contains information about paths to labels and images from the IAM database."""

    forms: Path
    lines: Path
    sentences: Path
    words: Path


class PathsConfig(BaseModel):
    """Contains configuration for path used in project."""

    data: Path
    images: DatabasePathsConfig
    labels: DatabasePathsConfig
    models: PathsModels


class ParamsConfig(BaseModel):
    """Contains information about parameters for neural network."""

    batch_size: int
    epoch_count: int
    lr: float
    random_state: int
    train_size: float
    test_size: float
    dropout: float
    word_image_height: int
    word_image_width: int


class OCRConfig(BaseModel):
    """Project configuration."""

    params: ParamsConfig
    paths: PathsConfig
