"""Custom CTC loss."""

from typing import Any

import torch
from torch import nn


class CTCLoss(nn.Module):
    """Custom CTC loss definition."""

    def __init__(self, blank_char: int) -> None:
        super().__init__()

        self.blank_char = blank_char
        self.ctc = nn.CTCLoss(blank=blank_char, reduction="mean", zero_infinity=False)

    def forward(self, output: torch.Tensor, target: torch.Tensor) -> Any:
        # Remove padding and blank tokens from target
        target_lengths = torch.sum(target != self.blank_char, dim=1)
        target_unpadded = target[target != self.blank_char].view(-1)

        output = output.permute(1, 0, 2)  # (sequence_length, batch_size, num_classes)
        output_lengths = torch.full(
            size=(output.size(1),),
            fill_value=output.size(0),
            dtype=torch.int64,
        )

        return self.ctc(output, target_unpadded, output_lengths, target_lengths)
