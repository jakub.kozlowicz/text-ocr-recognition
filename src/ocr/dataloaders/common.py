"""Common functionalities across loaders."""

from dataclasses import dataclass
from enum import StrEnum
from pathlib import Path
from typing import TypeVar

from pydantic import BaseModel


@dataclass
class BoundingBox:
    x: int
    y: int
    w: int
    h: int


class CorrectWriting(StrEnum):
    """Represents whether writing in image is ok or error.

    Error variant must have value of "err" as it is specified in words.txt file
    which contains information about words labels.
    """

    OK = "ok"
    ERROR = "err"


class DBInfo(BaseModel):
    """Base database information about dataset."""

    identifier: str
    correct: CorrectWriting
    gray_level: int
    bounding_box: BoundingBox
    image_path: Path
    text: str


T = TypeVar("T", bound=DBInfo)


def exclude_wrong_data(db: list[T]) -> list[T]:
    """Excludes wrong data from dataset."""
    return [info for info in db if info.correct == CorrectWriting.OK]


def extract_lines(file: Path) -> list[str]:
    """Extracts lines from file."""
    return [
        line.strip()
        for line in file.read_text(encoding="utf-8").splitlines()
        if not line.startswith("#")
    ]


def construct_image_path_from_identifier(identifier: str) -> Path:
    """Builds image path from identifier."""
    first, second = identifier.split("-")[0:2]
    return Path(f"{first}/{first}-{second}/{identifier}.png")
