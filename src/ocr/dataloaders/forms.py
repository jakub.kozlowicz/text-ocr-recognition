"""Database information loader for forms images."""

from enum import StrEnum
from pathlib import Path

from pydantic import BaseModel

from .common import extract_lines


class SegmentationStatus(StrEnum):
    """Status of image segmentation."""

    PARTIAL = "prt"
    ALL = "all"


class FormsDb(BaseModel):
    """Forms database information."""

    form_id: str
    segmentation_status: SegmentationStatus
    no_lines: int
    image_path: Path


def loads_forms_database(labels_file: Path, images_path: Path) -> list[FormsDb]:
    """Loads information about forms images."""
    images_db: list[FormsDb] = []
    lines = extract_lines(labels_file)

    for line in lines:
        line_info = line.split(" ")
        form_id = line_info[0]
        segmentation_status = SegmentationStatus(line_info[3])
        no_lines = line_info[4]
        image_path = images_path / f"{line_info[0]}.png"
        if segmentation_status == SegmentationStatus.ALL:
            images_db.append(
                FormsDb(
                    form_id=form_id,
                    segmentation_status=segmentation_status,
                    no_lines=int(no_lines),
                    image_path=image_path,
                ),
            )

    return images_db
