"""Lines loader for the IAM database."""

from collections.abc import Callable
from pathlib import Path

from .common import BoundingBox, CorrectWriting, DBInfo, extract_lines


class LinesInfo(DBInfo):
    """Structure of the information in one line for the lines database."""

    no_tokens: int


def loads_lines_database(
    labels_file: Path,
    images_path: Path,
    prepare_path: Callable[[str], Path],
) -> list[LinesInfo]:
    """Loads words information."""
    images_db: list[LinesInfo] = []
    lines = extract_lines(labels_file)

    for line in lines:
        line_iter = iter(line.split(" "))
        identifier = next(line_iter)
        info = LinesInfo(
            identifier=identifier,
            correct=CorrectWriting(next(line_iter)),
            gray_level=int(next(line_iter)),
            no_tokens=int(next(line_iter)),
            bounding_box=BoundingBox(
                int(next(line_iter)),
                int(next(line_iter)),
                int(next(line_iter)),
                int(next(line_iter)),
            ),
            text=("".join(list(line_iter))).replace("|", " "),
            image_path=images_path / prepare_path(identifier),
        )
        images_db.append(info)

    return images_db
