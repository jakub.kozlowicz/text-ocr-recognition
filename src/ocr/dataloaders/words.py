"""Words loader for the IAM database."""

from collections.abc import Callable
from pathlib import Path

from .common import BoundingBox, CorrectWriting, DBInfo, extract_lines


class WordsInfo(DBInfo):
    """Structure of the information in one line for the words database."""

    tag: str


def loads_words_database(
    labels_file: Path,
    images_path: Path,
    prepare_path: Callable[[str], Path],
) -> list[WordsInfo]:
    """Loads words information."""
    images_db: list[WordsInfo] = []
    lines = extract_lines(labels_file)

    for line in lines:
        line_iter = iter(line.split(" "))
        identifier = next(line_iter)
        info = WordsInfo(
            identifier=identifier,
            correct=CorrectWriting(next(line_iter)),
            gray_level=int(next(line_iter)),
            bounding_box=BoundingBox(
                int(next(line_iter)),
                int(next(line_iter)),
                int(next(line_iter)),
                int(next(line_iter)),
            ),
            tag=next(line_iter),
            text=" ".join(list(line_iter)),
            image_path=images_path / prepare_path(identifier),
        )
        images_db.append(info)

    return images_db
