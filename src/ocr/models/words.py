"""Words prediction model."""

from pathlib import Path

import cv2
import numpy as np
from numpy.typing import NDArray
import onnxruntime as ort

from ocr.text_utils import ctc_decoder


class WordsModel:
    """Words prediction model."""

    def __init__(self, model: Path):
        self.model_path = model

        if not model.exists():
            msg = f"Model path ({model}) does not exist"
            raise RuntimeError(msg)

        providers = (
            ["CUDAExecutionProvider", "CPUExecutionProvider"]
            if ort.get_device() == "GPU"
            else ["CPUExecutionProvider"]
        )

        self.model = ort.InferenceSession(self.model_path, providers=providers)

        metadata = self.model.get_modelmeta().custom_metadata_map
        if metadata:
            self.vocab = metadata.get("vocab", "")

        self.input_shape = self.model.get_inputs()[0].shape[1:]

        if self.model._inputs_meta is None:
            raise ValueError

        self.input_name = self.model._inputs_meta[0].name

    def predict(self, image: NDArray[np.uint8]) -> str:
        image = cv2.resize(image, self.input_shape[:2][::-1])
        image_pred = np.expand_dims(image, axis=0).astype(np.float32)
        preds = self.model.run(None, {self.input_name: image_pred})[0]
        return ctc_decoder(preds, self.vocab)[0]
