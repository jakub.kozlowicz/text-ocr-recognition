"""Auxilary functions for text processing."""

from itertools import groupby

import numpy as np
from numpy.typing import NDArray


def remove_spaces_before_punctuation(text: str) -> str:
    """Remove spaces before punctuation."""
    punctuation = [".", ",", "!", "?", ":", ";", ")", "]", "}", "»"]
    for p in punctuation:
        text = text.replace(f" {p}", p)
    return text


def ctc_decoder(predictions: NDArray[np.uint8], vocabulary: str | list[str]) -> list[str]:
    """CTC greedy decoder for predictions"""
    argmax_preds = np.argmax(predictions, axis=-1)
    grouped_preds = [[k for k, _ in groupby(preds)] for preds in argmax_preds]
    return [
        "".join([vocabulary[k] for k in group if k < len(vocabulary)]) for group in grouped_preds
    ]
