"""Custom accuracy module."""

import torch


class Accuracy:
    """Custom accuracy class."""

    def __init__(self) -> None:
        self.total = 0
        self.correct = 0

    def update(self, prediction: torch.Tensor, target: torch.Tensor) -> None:
        """Update metrics after prediction."""
        _, predicted = torch.max(prediction.data, 1)
        self.total += target.size(0)
        self.correct += (predicted == target).sum().item()

    def result(self) -> float:
        """Return resulted accuracy."""
        return self.correct / self.total

    def reset(self) -> None:
        """Reset metrics values."""
        self.total = 0
        self.correct = 0
