"""Custom error rate metrics."""

from abc import abstractmethod
from itertools import groupby
from typing import Any

import torch
from torchmetrics import Metric

from .utils.text import get_cer, get_wer


class ErrorRate(Metric):
    """Base class for calculating error rate metric."""

    higher_is_better = False

    def __init__(self, vocabulary: str) -> None:
        super().__init__()
        self.vocabulary = vocabulary
        self.add_state(
            "error_rate",
            default=torch.tensor(0.0, dtype=torch.float32),
            dist_reduce_fx="sum",
        )
        self.add_state("total", default=torch.tensor(0), dist_reduce_fx="sum")

    def update(self, prediction: torch.Tensor, target: torch.Tensor) -> None:
        """Update metrics values."""
        argmax_preds = torch.argmax(prediction, dim=-1)

        grouped_preds = [[k for k, _ in groupby(preds)] for preds in argmax_preds]
        output_texts = self.get_texts(grouped_preds)
        target_texts = self.get_texts(target)

        error = self.get_error(output_texts, target_texts)

        self.error_rate += torch.tensor(error)
        self.total += torch.tensor(1)

    def compute(self) -> Any:
        """Return resulted error rate."""
        if isinstance(self.error_rate, torch.Tensor) and isinstance(self.total, torch.Tensor):
            return self.error_rate.float() / self.total

        return 0.0

    def get_texts(self, groups: list[list[torch.Tensor]] | torch.Tensor) -> list[str]:
        return [
            "".join([self.vocabulary[k] for k in group if k < len(self.vocabulary)])
            for group in groups
        ]

    @abstractmethod
    def get_error(self, output_texts: list[str], target_texts: list[str]) -> float:
        msg = "Subclasses must implement get_error method."
        raise NotImplementedError(msg)


class CharErrorRate(ErrorRate):
    """Custom character error rate metric."""

    def get_error(self, output_texts: list[str], target_texts: list[str]) -> float:
        return get_cer(output_texts, target_texts)


class WordErrorRate(ErrorRate):
    """Custom word error rate metric."""

    def get_error(self, output_texts: list[str], target_texts: list[str]) -> float:
        return get_wer(output_texts, target_texts)
