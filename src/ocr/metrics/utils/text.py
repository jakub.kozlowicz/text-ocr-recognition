"""Auxilary functions used in text error rates."""


def edit_distance(prediction_tokens: list[str], reference_tokens: list[str]) -> int:
    """Standard dynamic programming algorithm to compute the Levenshtein Edit Distance Algorithm"""
    m, n = len(prediction_tokens), len(reference_tokens)

    # Initialize a matrix to store the edit distances
    dp = [[0] * (n + 1) for _ in range(m + 1)]

    # Fill the first row and column with the number of insertions needed
    for i in range(m + 1):
        dp[i][0] = i
    for j in range(n + 1):
        dp[0][j] = j

    # Iterate through the prediction and reference tokens
    for i, p_tok in enumerate(prediction_tokens, 1):
        for j, r_tok in enumerate(reference_tokens, 1):
            # If the tokens are the same, no change is needed
            if p_tok == r_tok:
                dp[i][j] = dp[i - 1][j - 1]

            # If the tokens are different, take the minimum cost
            else:
                dp[i][j] = min(dp[i - 1][j], dp[i][j - 1], dp[i - 1][j - 1]) + 1

    return dp[-1][-1]  # final entry in the matrix as the edit distance


def get_cer(prediction: str | list[str], target: str | list[str]) -> float:
    """Calculate the Character Error Rate (CER) for a list of predictions and targets."""
    if isinstance(prediction, str):
        prediction = [prediction]

    if isinstance(target, str):
        target = [target]

    total, errors = 0, 0
    for pred_tokens, target_tokens in zip(prediction, target, strict=True):
        errors += edit_distance(list(pred_tokens), list(target_tokens))
        total += len(target_tokens)

    return errors / total if total != 0 else 0.0


def get_wer(prediction: str | list[str], target: str | list[str]) -> float:
    """Calulate the Word Error Rate (WER) for a list of predictions and targets."""
    if isinstance(prediction, str):
        prediction = prediction.split()

    if isinstance(target, str):
        target = target.split()

    errors = edit_distance(prediction, target)
    total_words = len(target)

    return errors / total_words if total_words != 0 else 0.0
