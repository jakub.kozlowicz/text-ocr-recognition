"""Images transformations for the HTR module."""

from typing import Any

from torch import Tensor
from torchvision.transforms.functional import pad, resize


class ResizeImage:
    """Resizes image to given dimensions keeping aspect ratio."""

    def __init__(self, height: int, width: int, color: int = 0) -> None:
        self.height = height
        self.width = width
        self.color = color

    def calculate_new_dimensions(self, input_height: int, input_width: int) -> tuple[int, int]:
        """Calculate dimentions of the new image"""
        ratio = min(self.width / input_width, self.height / input_height)
        return int(input_height * ratio), int(input_width * ratio)

    def calculate_padding(self, new_height: int, new_width: int) -> tuple[int, int, int, int]:
        """Calculate padding to match target dimensions."""
        delta_w = self.width - new_width
        delta_h = self.height - new_height
        top, bottom = delta_h // 2, delta_h - (delta_h // 2)
        left, right = delta_w // 2, delta_w - (delta_w // 2)
        return left, top, right, bottom

    def __call__(self, x: Tensor) -> Any:
        *_, input_height, input_width = x.size()

        # Resize image keeping aspect ratio
        new_height, new_width = self.calculate_new_dimensions(input_height, input_width)
        x = resize(x, [new_height, new_width], antialias=True)

        # Calculate padding for image to fill the target dimensions
        padding = self.calculate_padding(new_height, new_width)
        return pad(x, [*padding], fill=self.color)
