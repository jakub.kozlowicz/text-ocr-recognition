"""Labels transformations for the HTR module."""

from torch import Tensor
from torch.nn.functional import pad


class LabelPadder:
    """Pads label to the desired width."""

    def __init__(self, width: int, value: int = 0) -> None:
        self.width = width
        self.value = value

    def __call__(self, x: Tensor) -> Tensor:
        return pad(x, (0, self.width - x.size(0)), value=self.value)
