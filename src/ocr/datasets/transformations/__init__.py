"""Base transformation for dataset"""

from typing import Any, Protocol

import torch


class Transformation(Protocol):
    def __call__(self, x: torch.Tensor) -> Any:
        """Transformation function."""
