"""Labels encoder and decoder for IAM dataset."""


class LabelEncoder:
    """Labels coder to change string labels into numerical values."""

    CLASSES = '_!"#&\'()*+,-./0123456789:;?ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz '

    def __init__(self, classes: str = CLASSES) -> None:
        self.c_to_int: dict[str, int] = dict(zip(classes, range(len(classes)), strict=True))
        self.int_to_c: dict[int, str] = dict(zip(range(len(classes)), classes, strict=True))

    def encode(self, label: str) -> list[int]:
        """Encode string representation of the label to the numerical representation."""
        return [self.c_to_int[character] for character in label]

    def decode(self, encoded: list[int]) -> str:
        """Decode numerical representation of the label to the string representation."""
        return "".join([self.int_to_c[value] for value in encoded])

    @property
    def num_classes(self) -> int:
        """Returns the number of classes in labels."""
        return len(self.CLASSES)

    @property
    def blank_char_value(self) -> int:
        """Return value of the blank character."""
        return self.encode(" ")[0]
