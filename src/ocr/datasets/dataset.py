"""Dataset for pytorch network module."""

from typing import Any

import torch
from torch.utils.data import Dataset
from torchvision.io import ImageReadMode, read_image

from ocr.dataloaders.common import T

from .labels_encoder import LabelEncoder
from .transformations import Transformation


class HtrDataset(Dataset[Any]):
    """Handwritten words dataset."""

    def __init__(
        self,
        data: list[T],
        image_transformations: list[Transformation],
        labels_transformations: list[Transformation],
        encoder: LabelEncoder,
    ) -> None:
        super().__init__()

        self._data = data
        self._length = len(data)

        self.image_transformations = image_transformations
        self.labels_transformations = labels_transformations

        self.encoder = encoder

    def __len__(self) -> int:
        """Returns the length of the dataset."""
        return self._length

    def __getitem__(self, index: int) -> tuple[torch.Tensor, torch.Tensor]:
        """Gets data and label at given index."""
        word_info = self._data[index]

        image = read_image(str(word_info.image_path), mode=ImageReadMode.GRAY) / 255.0
        label = torch.tensor(self.encoder.encode(word_info.text), dtype=torch.uint8)

        for transformation in self.image_transformations:
            image = transformation(image)

        for transformation in self.labels_transformations:
            label = transformation(label)

        return image, label
