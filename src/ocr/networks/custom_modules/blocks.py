"""Custom block modules for the HTR network."""

from typing import Any

import torch
from torch import nn


class Convolutional(nn.Module):
    def __init__(  # noqa: PLR0913
        self,
        in_channels: int,
        out_channels: int,
        kernel_size: int,
        stride: int,
        padding: int,
    ) -> None:
        super().__init__()

        self.module = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
            ),
            nn.BatchNorm2d(num_features=out_channels),
        )

    def forward(self, x: torch.Tensor) -> Any:
        return self.module(x)


class Residual(nn.Module):
    def __init__(  # noqa: PLR0913
        self,
        in_channels: int,
        out_channels: int,
        shortcut: nn.Module | None = None,
        stride: int = 1,
        dropout: float = 0.2,
    ) -> None:
        super().__init__()

        self.module = nn.Sequential(
            Convolutional(in_channels, out_channels, kernel_size=3, stride=stride, padding=1),
            nn.LeakyReLU(negative_slope=0.1, inplace=True),
            Convolutional(out_channels, out_channels, kernel_size=3, stride=1, padding=1),
        )
        self.dropout = nn.Dropout(p=dropout)
        self.shortcut = shortcut if shortcut else None
        self.activation = nn.LeakyReLU(negative_slope=0.1, inplace=True)

    def forward(self, x: torch.Tensor) -> Any:
        skip = x
        x = self.module(x)

        if self.shortcut is not None:
            x += self.shortcut(skip)

        x = self.activation(x)
        return self.dropout(x)
