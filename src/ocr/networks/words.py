"""HTR module for recognizing individual words."""

from typing import Any

import torch
from torch import nn

from .custom_modules.blocks import Residual


class WordsHtrModule(nn.Module):
    """Handwritten words recognition module for CTC loss."""

    def __init__(self, num_classes: int, dropout: float = 0.2) -> None:
        super().__init__()

        self.convolutional = nn.Sequential(
            Residual(
                in_channels=3,
                out_channels=16,
                shortcut=nn.Conv2d(3, 16, kernel_size=1, stride=1),
                stride=1,
                dropout=dropout,
            ),
            Residual(
                in_channels=16,
                out_channels=16,
                shortcut=nn.Conv2d(16, 16, kernel_size=1, stride=2),
                stride=2,
                dropout=dropout,
            ),
            Residual(
                in_channels=16,
                out_channels=16,
                stride=1,
                dropout=dropout,
            ),
            Residual(
                in_channels=16,
                out_channels=32,
                shortcut=nn.Conv2d(16, 32, kernel_size=1, stride=2),
                stride=2,
                dropout=dropout,
            ),
            Residual(
                in_channels=32,
                out_channels=32,
                stride=1,
                dropout=dropout,
            ),
            Residual(
                in_channels=32,
                out_channels=64,
                shortcut=nn.Conv2d(32, 64, kernel_size=1, stride=2),
                stride=2,
                dropout=dropout,
            ),
            Residual(
                in_channels=64,
                out_channels=64,
                stride=1,
                dropout=dropout,
            ),
            Residual(
                in_channels=64,
                out_channels=64,
                stride=1,
                dropout=dropout,
            ),
            Residual(
                in_channels=64,
                out_channels=64,
                stride=1,
                dropout=dropout,
            ),
        )

        self.lstm = nn.LSTM(64, 128, bidirectional=True, num_layers=1, batch_first=True)  # type: ignore
        self.lstm_dropout = nn.Dropout(p=dropout)

        self.fully_connected = nn.Linear(256, num_classes)
        self.log_softmax = nn.LogSoftmax(2)

    def forward(self, x: torch.Tensor) -> Any:
        # Convolutional
        x = self.convolutional(x)
        x = x.reshape(x.size(0), -1, x.size(1))

        # Recurrent
        x, _ = self.lstm(x)
        x = self.lstm_dropout(x)

        # Fully connected
        x = self.fully_connected(x)

        # Softmax
        return self.log_softmax(x)
