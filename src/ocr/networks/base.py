"""Base module for different network modules."""

from typing import Any

import pytorch_lightning as pl
import torch
from torch import nn

from ocr.metrics.error_rate import CharErrorRate


class BaseHtrModule(pl.LightningModule):
    """Base HTR module for different data."""

    def __init__(
        self,
        network: nn.Module,
        loss: nn.Module,
        vocabulary: str,
        lr: float = 1e-3,
    ) -> None:
        super().__init__()

        self.model = network
        self.loss = loss
        self.train_char_er = CharErrorRate(vocabulary)
        self.validation_char_er = CharErrorRate(vocabulary)
        self.lr = lr

    def training_step(self, batch: list[Any], _: int) -> Any:
        data, target = batch
        prediction = self.model(data)
        loss = self.loss(prediction, target)
        self.train_char_er(prediction, target)
        return loss

    def on_train_epoch_end(self) -> None:
        self.log("Training CharErrorRate", self.train_char_er.compute(), on_epoch=True, logger=True)

    def validation_step(self, batch: list[Any], _: int) -> Any:
        data, target = batch
        prediction = self.model(data)
        loss = self.loss(prediction, target)
        self.validation_char_er(prediction, target)
        return loss

    def on_validation_epoch_end(self) -> None:
        self.log(
            "Validation CharErrorRate",
            self.validation_char_er.compute(),
            on_epoch=True,
            logger=True,
        )

    def configure_optimizers(self) -> torch.optim.Optimizer:
        return torch.optim.Adam(self.parameters(), lr=self.lr)
