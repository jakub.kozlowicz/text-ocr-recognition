"""Custom detector functions for images."""

from collections import namedtuple

import cv2
import numpy as np
from numpy.typing import NDArray


Line = namedtuple("Line", ["x", "y", "w", "h"])
CONTOUR_AREA_THRESHOLD = 7
CONTOUR_HEIGHT_THRESHOLD = 50
CONTOUR_WIDTH_THRESHOLD = 500


def detect_lines(image: NDArray[np.uint8]) -> list[Line]:
    """Detects horizontal lines in the image."""
    horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (150, 1))
    detect_horizontal = cv2.morphologyEx(
        image,
        cv2.MORPH_OPEN,
        horizontal_kernel,
        iterations=2,
    )
    contours = cv2.findContours(detect_horizontal, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[0]
    return [Line(*cv2.boundingRect(contour)) for contour in contours]


def _valid_contour(contour: NDArray[np.int32]) -> bool:
    """Validates detected contour."""
    *_, width, height = cv2.boundingRect(contour)
    return bool(
        cv2.contourArea(contour) > CONTOUR_AREA_THRESHOLD
        and height > CONTOUR_HEIGHT_THRESHOLD
        and width > CONTOUR_WIDTH_THRESHOLD
        and height < width,
    )


def detect_text_lines(image: NDArray[np.uint8]) -> list[NDArray[np.int32]]:
    """Detects lines of handwritten text."""
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (100, 3))
    dilated = cv2.dilate(image, kernel, iterations=2)
    conts = cv2.findContours(dilated.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[0]
    contours = [contour for contour in conts if _valid_contour(contour)]
    # Sort lines from top to botom
    return sorted(contours, key=lambda ctr: cv2.boundingRect(ctr)[1])  # type: ignore


def detect_words_in_line(image: NDArray[np.uint8]) -> list[NDArray[np.int32]]:
    """Detects individual words in line of text."""
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (10, image.shape[0]))
    dilated = cv2.dilate(image, kernel, iterations=2)
    contours = cv2.findContours(dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[0]
    # Sort words from left to right
    return sorted(contours, key=lambda ctr: cv2.boundingRect(ctr)[0])  # type: ignore
