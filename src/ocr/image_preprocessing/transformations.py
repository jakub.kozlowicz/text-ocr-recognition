"""Custom transformations for the images."""

import cv2
import numpy as np
from numpy.typing import NDArray


def convert_to_grayscale(image: NDArray[np.uint8]) -> NDArray[np.uint8]:
    """Converts image to grayscale."""
    return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


def binarize(image: NDArray[np.uint8]) -> NDArray[np.uint8]:
    """Binarizes image and return new image with values [0, 255]."""
    return cv2.threshold(image, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]


def denoise(image: NDArray[np.uint8]) -> NDArray[np.uint8]:
    """Appies denoising filter to the image."""
    return cv2.fastNlMeansDenoising(image, h=10, templateWindowSize=7, searchWindowSize=21)


def draw_line(
    image: NDArray[np.uint8],
    start_point: tuple[int, int],
    end_point: tuple[int, int],
    color: tuple[int, int, int] = (0, 255, 0),
    thickness: int = 2,
) -> NDArray[np.uint8]:
    """Draws line on the image."""
    return cv2.line(image, start_point, end_point, color, thickness)  # type: ignore


def split_image_vertically(image: NDArray[np.uint8], splits: list[int]) -> list[NDArray[np.uint8]]:
    """Splits image vertically into multiple images."""
    cropped_images = []

    for i in range(len(splits) - 1):
        y1 = splits[i]
        y2 = splits[i + 1]

        cropped_image = image[y1:y2, :]
        cropped_images.append(cropped_image)

    y = splits[-1]
    cropped_image = image[y:, :]
    cropped_images.append(cropped_image)

    return cropped_images


def crop_image(
    image: NDArray[np.uint8],
    x: int,
    y: int,
    width: int,
    height: int,
) -> NDArray[np.uint8]:
    """Crops images to desired dimensions."""
    return image[y : y + height, x : x + width]


def extract_from_contours(
    image: NDArray[np.uint8],
    contours: list[NDArray[np.int32]],
) -> list[NDArray[np.uint8]]:
    """Crops image by contours and return cropped images."""
    result: list[NDArray[np.uint8]] = []
    for ctr in contours:
        x, y, w, h = cv2.boundingRect(ctr)
        cropped_line = crop_image(image.copy(), x, y, w, h)
        result.append(cropped_line)

    return result
