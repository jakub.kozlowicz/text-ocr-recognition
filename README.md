# Text OCR recognition

[[_TOC_]]

Simple Artificial Neural Network to recognize handwritten text. Written with
PyTorch. Trained and tested with the IAM dataset.

## Acknowledgements

Project written during course called "Intelligent virtualisation of systems and
process automation" at **Wroclaw University of Science and Technology**.

## Author

Designed and created by [Jakub Kozłowicz](https://kozlowicz.io)

## License

Licensed under MIT license. See LICENSE file for more information.
